# Witcurve  
##### UI Guidelines
 ### Color Palatee
They are 1 major and 1 secondary and 4 tertiary Colors 
# Major Color
  - Hex code : #133a5f 
# Secondary Color
  - Hex code : #A8D0DC
# Tertiary Colors
  - Hex code : #41822F
  - Hex code : #C54245
  - Hex code : #FFFFFF
  - Hex code : #00000 
# Font Details
  - Font name  : poppins
  - Font sizes : 13px, 8px, 18px,10px
  - font weight : Regular, Medium , Bold
  - Font color : Black (#00000)

# Table design
 - Border color  : Black (#00000)
 - Border radius : 2

##### Header
  - font-size  : 13px
  - Font- weight : Bold
  - font color : White ( #FFFFFF )
 ##### Content 
  - Font size - 10px
  - Font - weight : Medium
  - Font Color : Black (#00000)
  - 
  ##### Paginator 
  - Font size - 10px
  - Font - weight : Medium
  - Font Color : Black (#00000) (numbers)
  - background color : #A8D0DC
  - Border color : Black (#00000)
  - Border radius : 2

# Side navbar 
#### Header
- Font size - 18px
- Font weight - Bold 
- font color - White
#### Sub Header (Submenu)
- Font size - 12px
- Font Weight - Medium
- Font color : White 
- background color : #A8D0DC 

# Breadcrumb
- Font -size : 13px
- Font -weight : Regular
- Font - color : #133a5f

